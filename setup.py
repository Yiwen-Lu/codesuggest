#!/usr/bin/env python
"""This script prompts a user to build and distribute packages"""

from setuptools import setup, find_namespace_packages

setup(
    name="CodeSuggest",
    version="0.1",
    packages=find_namespace_packages(),
)
