from string import punctuation
from typing import Optional, List, Union
from tokenizers import Tokenizer, AddedToken, decoders, trainers
from tokenizers.models import WordPiece
from tokenizers.normalizers import BertNormalizer
from tokenizers.pre_tokenizers import WhitespaceSplit, BertPreTokenizer
from tokenizers.implementations.base_tokenizer import BaseTokenizer


class WordPieceTokenizer(BaseTokenizer):
    """Adaptation of WordPiece Tokenizer with special handling of punctuation.

    This WordPiece Tokenizer can be be trained on a corpus to build a vocabulary
    based on the frequency in corpus. Punctuation tokens, however, will never be
    merged with other tokens in this process.

    Attributes:
        wordpieces_prefix: String prefix that will be prepended to tokens to
            indicate that a token is a continuation of a word.

        pad_token (str): Padding token.
        pad_token_id (int): Padding token id.
        eol_token (str): End-of-Line token.
        eol_token_id (int): End-of-Line token id.
        unk_token (str): Unknown token. Used when word can't be represented
            with vocabulary.
        unk_token_id (int): Unknown token id.

        special_token_list (List[str]): List of all special tokens.

        special_token_id_list (List[int]): List of all special token ids.

    """

    # The interfaces are the same as in huggingface
    # pylint: disable=too-many-arguments
    # In this case 9 attributes seems reasonable
    # pylint: disable=too-many-instance-attributes
    def __init__(
                self,
                vocab_file: Optional[str] = None,
                pad_token: Union[str, AddedToken] = "[PAD]",
                eol_token: Union[str, AddedToken] = "[EOL]",
                unk_token: Union[str, AddedToken] = "[UNK]",
                clean_text: bool = True,
                handle_chinese_chars: bool = True,
                strip_accents: bool = True,
                lowercase: bool = False,
                wordpieces_prefix: str = "##",
            ):
        """Initializes class either for training or from a vocabfile.

        Args:
            vocab_file: Filepath to vocab file that defines the tokens. (Default: None)
            pad_token: Token used as padding token. (Default: "[PAD]")
            eol_token: Token used as end-of-line token. (Default: "[EOL]")
            unk_token: Token used as unknown token. (Default: "[UNK]")
            clean_text: If True, cleans command characters from
                input. (Default: True)
            handle_chinese_chars: If True, includes chinese chars in
                vocabulary. (Default: True)
            strip_accents: If True, cleans accents from input. (Default: True)
            lowercase: If True, all input will be handled as lowercase. (Default: False)
            wordpieces_prefix: String prefix that will be prepended to tokens to
                indicate that a token is a continuation of a word. (Default: "##")

        """
        if vocab_file is not None:
            tokenizer = Tokenizer(WordPiece(vocab_file, unk_token=str(unk_token)))
        else:
            tokenizer = Tokenizer(WordPiece())

        # Let the tokenizer know about special tokens if they are part of the vocab
        if tokenizer.token_to_id(str(pad_token)) is not None:
            tokenizer.add_special_tokens([str(pad_token)])
        if tokenizer.token_to_id(str(eol_token)) is not None:
            tokenizer.add_special_tokens([str(eol_token)])
        if tokenizer.token_to_id(str(unk_token)) is not None:
            tokenizer.add_special_tokens([str(unk_token)])

        tokenizer.normalizer = BertNormalizer(
            clean_text=clean_text,
            handle_chinese_chars=handle_chinese_chars,
            strip_accents=strip_accents,
            lowercase=lowercase,
        )

        tokenizer.pre_tokenizer = WhitespaceSplit()

        tokenizer.decoder = decoders.WordPiece(prefix=wordpieces_prefix)

        parameters = {
            "model": "BertWordPiece",
            "pad_token": pad_token,
            "eol_token": eol_token,
            "unk_token": unk_token,
            "clean_text": clean_text,
            "handle_chinese_chars": handle_chinese_chars,
            "strip_accents": strip_accents,
            "lowercase": lowercase,
            "wordpieces_prefix": wordpieces_prefix,
        }

        super().__init__(tokenizer, parameters)

        self.wordpieces_prefix = wordpieces_prefix

        self.pad_token = pad_token
        self.pad_token_id = self.token_to_id(pad_token)
        self.eol_token = eol_token
        self.eol_token_id = self.token_to_id(eol_token)
        self.unk_token = unk_token
        self.unk_token_id = self.token_to_id(unk_token)

        self.special_token_list = [
            self.pad_token,
            self.eol_token,
            self.unk_token
            ]

        self.special_token_id_list = [
            self.pad_token_id,
            self.eol_token_id,
            self.unk_token_id
            ]
        # pylint: enable=too-many-instance-attributes

    def train(
        self,
        files: Union[str, List[str]],
        vocab_size: int = 52000,
        min_frequency: int = 2,
        limit_alphabet: int = 1000,
        initial_alphabet: Optional[List[str]] = None,
        special_tokens: Optional[List[Union[str, AddedToken]]] = None,
        show_progress: bool = True,
        wordpieces_prefix: str = "##",
    ):
        """Train the model using the given files as corpus

        Generates a vocabulary from a frequency analysis of a corpus of text.

        Args:
            files: Filepaths to the files used as the corpus.
            vocab_size: The size of the generated vocabulary. (Default: 52000)
            min_frequency: The minimun number of times that a token needs to
                appear in the corpus before it can become part of the
                vocabulary. (Default: 2)
            limit_alphabet: Maximum number of alphabet tokens
                (One character tokens). (Default: 1000)
            initial_alphabet: The initial alphabet. These tokens don't need to
                appear in the corpus to become part of the vocabulary. (Default: None)
            special_tokens: List of special tokens to be considered during
                generation. Special tokens will never be merged. (Default: None)
            show_progress: If True, shows progress during generation. (Default: True)
            wordpieces_prefix: String prefix that will be prepended to tokens to
                indicate that a token is a continuation of a word. (Default: "##")

        """
        if initial_alphabet is None:
            initial_alphabet = []

        if special_tokens is None:
            special_tokens = self.special_token_list

        special_tokens += ["##" + char for char in punctuation]

        trainer = trainers.WordPieceTrainer(
            vocab_size=vocab_size,
            min_frequency=min_frequency,
            limit_alphabet=limit_alphabet,
            initial_alphabet=initial_alphabet,
            special_tokens=special_tokens,
            show_progress=show_progress,
            continuing_subword_prefix=wordpieces_prefix,
        )

        if isinstance(files, str):
            files = [files]

        # change pre_tokenizer for training to make sure that punctuation is
        # never part of any merged tokens.
        self._tokenizer.pre_tokenizer = BertPreTokenizer()
        self._tokenizer.train(trainer, files)
        self._tokenizer.pre_tokenizer = WhitespaceSplit()

    # pylint: disable=unused-argument
    def get_special_tokens_mask(
                self, token_ids: List, already_has_special_tokens: bool = False
            ) -> List[int]:
        """Return a mask of the inputs special tokens.

        Args:
            token_ids: List of token ids.
            already_has_special_tokens: Ignored. Just to satisfy the interface.

        Returns:
            A list of integers in the range [0, 1]: 1 for a special token, 0 for a sequence token.

        """
        return [1 if token_id in self.special_token_id_list else 0 for token_id in token_ids]
    # pylint: enable=unused-argument

    def convert_tokens_to_ids(self, tokens:  Union[str, List[str]]):
        """ Converts a token string (or a sequence of tokens) into a single id
            (or a sequence of ids), using the vocabulary.

        Args:
            tokens: Single token or list of tokens.

        Returns:
            Token id or list of token ids of the input tokens.

        """
        if tokens is None:
            return None

        if isinstance(tokens, str):
            return self.token_to_id(tokens)

        return [self.token_to_id(token) for token in tokens]

    def __len__(self) -> int:
        """Return number of tokens in the vocabulary

        Returns:
            Number of tokens in the vocabulary.
        """
        return self.get_vocab_size()
