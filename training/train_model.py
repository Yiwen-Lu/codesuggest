"""This Script trains a TransfoXLLMHeadModel"""
import argparse
from transformers import TrainingArguments
from transformers import TransfoXLConfig, TransfoXLLMHeadModel
from training.trainer.trainer import Trainer
from training.collater.unwrap_collater import UnwrapCollater
from training.dataset.batched_iterable_dataset import BatchedIterableDataset
from tokenizer.tokenizer import WordPieceTokenizer


def main(args):
    """This is the main training script for the TransfoXL model.

    Training requires a vocab file, a tensorized data file and the corresponding
    tensorized index file.

    Args:
        args: Parsed commandline arguments.

    """
    tokenizer = WordPieceTokenizer(vocab_file=args.filepath_to_vocab)

    # Initialize a Transformer XL configuration with same vocab as tokenizer
    # Adaptive Softmax Cutoffs are scaled to the same relative value as the default
    configuration = TransfoXLConfig(
        vocab_size=52000,
        cutoffs=[3800, 7800, 38000],
        eos_token_id=tokenizer.eol_token_id,
        )

    model = TransfoXLLMHeadModel(config=configuration)
    model.tie_weights()

    train_dataset = BatchedIterableDataset(
        filepath_to_tensorized_index=args.filepath_to_tensorized_index,
        filepath_to_tensorized_data=args.filepath_to_tensorized_data,
        batch_size=args.batch_size,
        )

    training_args = TrainingArguments(
        output_dir=args.filepath_to_model,
        overwrite_output_dir=True,
        do_train=True,
        num_train_epochs=args.epochs,
        gradient_accumulation_steps=args.gradient_accumulation_steps,
        logging_steps=args.logging_steps,
        save_steps=10000,
        save_total_limit=2,
        past_index=2 if args.use_past else -1,
        per_device_train_batch_size=1  # Batching is done inside the dataset
        )

    collater = UnwrapCollater()

    trainer = Trainer(
        model=model,
        args=training_args,
        train_dataset=train_dataset,
        data_collator=collater,
        )

    trainer.train()

    trainer.save_model(args.filepath_to_model)


def parse_arguments():
    """Parses the commandline arguments.

    Returns:
        Arguments extracted from the commandline.

    """
    parser = argparse.ArgumentParser(description='train model')
    parser.add_argument('--filepath_to_tensorized_index', type=str,
                        default='../py150/tiny_txt_tensorized_index.pt',
                        help='path to index file for tensorized data')
    parser.add_argument('--filepath_to_tensorized_data', type=str,
                        default='../py150/tiny_txt_tensorized_data.pt',
                        help='path to tensorized data file')
    parser.add_argument('--filepath_to_vocab', type=str,
                        default='tokenizer/PythonP150-vocab.txt',
                        help='path to vocab txt file')
    parser.add_argument('--filepath_to_model', type=str,
                        default='model',
                        help='path to directory of model')
    parser.add_argument('--logging_steps', type=int,
                        default=250,
                        help='logging intervall for tensorboard')
    parser.add_argument('--gradient_accumulation_steps', type=int,
                        default=10,
                        help='gradient accumulation steps')
    parser.add_argument('--epochs', type=int,
                        default=1,
                        help='epochs')
    parser.add_argument('--batch_size', type=int,
                        default=6,
                        help='batch size')
    parser.add_argument('--use_past', default=False, action='store_true',
                        help='Train using the past')
    args = parser.parse_args()

    return args


if __name__ == "__main__":
    main(parse_arguments())
