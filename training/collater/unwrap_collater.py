from typing import Dict, List
import torch


# This adapter needs only one methode to satisfy the interface
# pylint: disable=too-few-public-methods
class UnwrapCollater:
    """Unwraps already batched samples from list

    Serves as an adapter between batched_iterable_dataset and pytorch's
    default Dataset->Collater->Dataloader architecture.
    """
    def __call__(self, examples: List[torch.Tensor]) -> Dict[str, torch.Tensor]:
        """Unwraps already batched samples from list

        Expects len(examples) == 0 and examples[0]  to be a batch of samples.

        Args:
            examples: Only first element is used. First element is expected to
            be batch of samples.

        Returns:
            Batched samples.

            Returns same data as "input_ids" and "labels" as required by TransfoXL
            model.
        """
        batch = examples[0]
        return {"input_ids": batch, "labels": batch}
