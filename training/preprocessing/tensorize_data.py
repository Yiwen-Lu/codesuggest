"""Script to tensorize files

Basically CLI wrapper for Tensorizer class
"""
import argparse
import ntpath
import torch
from util.fileoperations import load_paths
from training.preprocessing.tensorizer.tensorizer import Tensorizer


def main(args):
    """Encode and chunk up files and create index. Save to tensor.

    Args:
        args: Parsed commandline arguments.

    """
    tensorizer = Tensorizer(
            filepath_to_vocab=args.filepath_to_vocab,
            chunksize=args.chunksize
        )
    paths_to_datafiles = load_paths(args.filepath_to_dataset, [args.filepath_to_indexfile])
    index, data = tensorizer.make_tensorized_index_and_data(
            paths_to_datafiles=paths_to_datafiles,
        )

    basename = ntpath.basename(args.filepath_to_indexfile).replace('.', '_')
    filename_data = basename + '_' + str(args.chunksize) + "_tensorized_data.pt"
    filename_index = basename + '_' + str(args.chunksize) + "_tensorized_index.pt"
    torch.save(index, args.filepath_to_dataset + "/" + filename_index)
    torch.save(data, args.filepath_to_dataset + "/" + filename_data)


def parse_arguments():
    """Parses the commandline arguments.

    Returns:
        Arguments extracted from the commandline.

    """
    parser = argparse.ArgumentParser(description='tensorize data and save to file')
    parser.add_argument('--filepath_to_indexfile', type=str,
                        default='py150/tiny.txt',
                        help='path to train set index file')
    parser.add_argument('--filepath_to_vocab', type=str,
                        default='tokenizer/PythonP150-vocab.txt',
                        help='path to vocab txt file')
    parser.add_argument('--filepath_to_dataset', type=str,
                        default='py150_files',
                        help='path to directory of dataset')
    parser.add_argument('--chunksize', type=int,
                        default=128,
                        help='chunksize used during interfile split up \
                            <=> seq length during training')
    args = parser.parse_args()

    return args


if __name__ == "__main__":
    main(parse_arguments())
