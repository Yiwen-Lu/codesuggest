class IterableConsumer:
    """Consumes Iterator that returns indexable objects.

    It will first iterate over the returned indexable object and when
    this object is consumed it will request the next object from the
    iterator.

    """

    def __init__(self, iterator):
        """Initializes dataset from other iterator

        Args:
            iterator: Iterator that iterates over indexable objects.

        """
        self._raw_data = iterator
        self._chunk = []
        self._head = 0

    def __iter__(self):
        """Initiate new iteration of this object

        Returns:
            This, ready to iterate from beginning.
        """
        self._chunk = []
        self._head = 0
        return self

    def __next__(self):
        """Return next item from within an indexable object

        Returns:
            Next item from within an indexable object.
        """
        if self._head >= len(self._chunk):
            self._refill_chunk()
        next_element = self._chunk[self._head]
        self._head += 1
        return next_element

    def _refill_chunk(self):
        """Consumes next object from internal iterator"""
        self._chunk = next(self._raw_data)
        self._head = 0
