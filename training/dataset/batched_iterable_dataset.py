from typing import List
import torch
from torch.utils.data import IterableDataset
from training.dataset.grouped_iterable_dataset import GroupedIterableDataset
from training.dataset.iterable_consumer import IterableConsumer


# IterableDatasets don't need to override __getitem__
# pylint: disable=abstract-method
class BatchedIterableDataset(IterableDataset):
    """This IterableDataset iterates over entire batches of data.

    Each index within a batch can be viewed as a channel and will yield
    sequencial data.

    """

    def __init__(
                self,
                filepath_to_tensorized_index: str,
                filepath_to_tensorized_data: str,
                batch_size=6
            ):
        """Initializes dataset from pre-tensorized data

        For more information on tensorization and its use
        see preprocessing and GroupedIterableDataset.

        Args:
            filepath_to_tensorized_index: Filepath to the saved tensor of index.
            filepath_to_tensorized_data: Filepath to the saved tensor of data.
            batch_size: Batch size for the returned batches. (Default: 6)

        Attributes:
            batch_size: Batch size for the returned batches.

        """
        self._raw_data = GroupedIterableDataset(
            filepath_to_tensorized_index,
            filepath_to_tensorized_data
            )

        self.batch_size = batch_size

        self._consumers = self._get_consumers()
        self._head = 0

    def __iter__(self) -> IterableDataset:
        """Initiate new iteration of this object

        Returns:
            This, ready to iterate from beginning.
        """
        self._consumers = self._get_consumers()
        self._head = 0
        return self

    def _get_consumers(self) -> List[IterableConsumer]:
        """Get new set of IterableConsumers.

        Returns:
            List of IterableConsumers. One for each Channel.
        """
        iterator = iter(self._raw_data)
        return [iter(IterableConsumer(iterator)) for i in range(self.batch_size)]

    def __next__(self) -> torch.Tensor:
        """Return next batch of samples

        Returns:
            Next batch of samples.
        """
        batch = []
        for consumer in self._consumers:
            next_element = next(consumer)
            batch += [next_element]
        batch = torch.stack(batch, dim=0)
        return batch

    def __len__(self) -> int:
        """Return number of samples in this iterator.

        This is actually an upper-bound. Iteration will actually
        halt if one of the channels (batch indeces) can not procure a new
        set of sequencial samples.

        Returns:
            Number of samples in this iterator.
        """
        return len(self._raw_data) // self.batch_size
