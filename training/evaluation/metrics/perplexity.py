"""Perplexity as metric function for evaluation"""
from math import exp
from transformers.trainer_utils import EvalPrediction


# eval_predictions is necessary to conform to interface
# pylint: disable=unused-argument
def perplexity(loss: float, eval_predictions: EvalPrediction):
    """Calculate Perplexity from loss

    In TransfoXL loss is cross-entropy

    Args:
        loss: Evaluation loss.
        predictions: Combined model output from entire evaluation dataset.

    Returns:
        Dict containing perplexity score.

    """
    return {"perplexity": exp(loss)}
