"""A collection of common fileoperations

This module offers a bunch of common fileoperations used during training
and testing
"""
import glob
from typing import List, Optional
from pathlib import Path


def is_comment(line: str) -> bool:
    """Return whether or not line is comment

    Comments are lines beginning with #

    Args:
        line: The line of text to be tested.

    Returns:
        True if comment, False if not.

    """
    return line[:1] == '#'


def get_filepaths_from_indexfile(indexfile: Path) -> List[str]:
    """Return paths contained in indexfile as list

    Args:
        indexfile: Path to a file that contains one filepath per line.

    Returns:
        List of filepaths to individual source files as str.

    """
    indexfile = open(Path(indexfile), mode='r', encoding="utf8")
    filepaths = indexfile.readlines()
    indexfile.close()

    return [str(Path(file.strip())) for file in filepaths if not is_comment(file)]


def compress_into_single_file(filepaths: List[str], outputfile: Path):
    """Compress files into one large file by concat

    Concatenates the file content of all files pointed to in filepaths into
    a single file.

    Args:
        filepaths: List of filepaths.
        outputfile: Path to combined output file. Will be created if not
            already present.

            Raises:
                OSError, IOError: If opening the file wasn't successful.

    """
    total = len(filepaths)
    count = 0
    with open(outputfile, mode='w', encoding="utf8") as outfile:
        for filepath in filepaths:
            count += 1
            if count % 500 == 0:
                print('Compressing... {:.2f}'.format(count/total))
            try:
                with open(filepath, encoding="utf8") as infile:
                    outfile.write(infile.read())
            except (OSError, IOError):
                print('Couldn\'t read file: ' + filepath)


def load_paths_from_directory(path_to_directory: Path) -> List[str]:
    """Return paths to all files in the given directory

    Return paths to all files in the given directory including its
    sub directories.

    Args:
        path_to_directory: Path to a directoy.

    Returns:
        List of filepaths to individual files in directory as str.

    """
    return glob.glob(str(path_to_directory) + '/**/*.*', recursive=True)


def load_paths_from_indexfiles(
            path_to_directory: Path,
            paths_to_index_files: List[Path]
        ) -> List[str]:
    """Return paths contained in indexfiles as list prepended by path_to_directory

    Args:
        path_to_directory: Path to a directoy that is root to all filepaths in the
            index files.
        paths_to_index_files: List of files that contains one filepath per line.

    Returns:
        List of filepaths to individual files as str.

    """
    return [str(Path(path_to_directory) / file) for path_to_index_file in paths_to_index_files
            for file in get_filepaths_from_indexfile(path_to_index_file)]


def load_paths(
            path_to_directory: Path,
            paths_to_index_files: Optional[List[Path]] = None
        ) -> List[str]:
    """Return paths contained in directory, possibly limited by indexfiles

    Args:
        path_to_directory: Path to a directory that is root to all filepaths in the
            index files.
        paths_to_index_files: List of files that contains one filepath per line.
            If no index files are passed all files in the directory are returned.

    Returns:
        List of filepaths to individual files as str.

    """
    if paths_to_index_files is None:
        return load_paths_from_directory(path_to_directory)
    return load_paths_from_indexfiles(path_to_directory, paths_to_index_files)
