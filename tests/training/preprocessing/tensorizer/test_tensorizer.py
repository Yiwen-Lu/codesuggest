from typing import List, Tuple
from pathlib import Path
import torch
from training.preprocessing.tensorizer.tensorizer import Tensorizer
from tests.util.test_fileoperations import TestFileoperations
from tokenizer.tokenizer import WordPieceTokenizer


class TestTensorizer(TestFileoperations):
    """Tests Tensorizer class."""

    def setUp(self):
        """Set up test. Will be called before each test."""
        super().setUp()
        self._tokenizer = WordPieceTokenizer(
            vocab_file=r"tokenizer/PythonP150-vocab.txt"
            )
        self._tensorizer = Tensorizer(
            filepath_to_vocab=r"tokenizer/PythonP150-vocab.txt",
            chunksize=self._chunksize
            )

    def _setup_temp_files(self):
        """Sets up temporary files for tests."""
        self._make_file(self.temp_dir, self._filename_a, self._file_content["file_a"])
        self._make_file(self.temp_dir, self._filename_b, self._file_content["file_b"])

    def test_make_tensorized_index_and_data(self):
        """Test tensorization process."""
        paths_to_datafiles = [
            self.temp_dir/self._filename_a,
            self.temp_dir/self._filename_b,
        ]
        output = self._tensorizer.make_tensorized_index_and_data(paths_to_datafiles)
        raw_encoding_a = self._raw_encoding(self.temp_dir, self._filename_a)
        raw_encoding_b = self._raw_encoding(self.temp_dir, self._filename_b)
        self._validate(output, raw_encoding_a, raw_encoding_b)

    def _raw_encoding(self, path: Path, filename: Path) -> List[int]:
        """Get token ids of the content of a given file.

        Args:
            path: Path to folder conataining filename.
            filename: Name of file to encode.

        Returns:
            The token ids equivalent to the file's content.
        """
        with open(path/filename, encoding="utf8") as file:
            return self._tokenizer.encode(file.read().replace('\n', '[EOL]')).ids

    def _validate(
                self,
                output: Tuple[torch.LongTensor, torch.LongTensor],
                raw_encoding_a: List[int],
                raw_encoding_b: List[int]
            ):
        """Validate that the chunks in output are equivalent to raw_encoding_A and B.

        Args:
            output: Index and Data tensor as returned by Tensorizer.
            raw_encoding_a: Token ids of first set of samples as indicated by index.
            raw_encoding_b: Token ids of second set of samples as indicated by index.
        """
        chunks_a, chunks_b = self._separate_chunks(output)
        self._validate_chunks(chunks_a, raw_encoding_a)
        self._validate_chunks(chunks_b, raw_encoding_b)

    def _validate_chunks(self, chunks: torch.LongTensor, raw_encoding: List[int]):
        """Validate that the chunks to the token ids in the raw_encoding.

        Args:
            chunks: The sample of one common index.
            raw_encoding: The token ids of that were used to create the chunks.
        """
        for i in range(len(chunks)-1):
            for j in range(len(chunks[i])):
                self.assertEqual(chunks[i][j], raw_encoding[i*self._chunksize+j])
        len_padding = self._chunksize*len(chunks)-len(raw_encoding)
        self._validate_padded_chunk(chunks[-1], raw_encoding, len_padding)

    def _validate_padded_chunk(
                self,
                chunk: torch.LongTensor,
                raw_encoding: List[int],
                len_padding: int
            ):
        """Validate chunk that contains padding tokens.

        Args:
            chunk: One sample that contains padding tokens.
            raw_encoding: The token ids used to create this sample. The sample
                comprises of the very last tokens in the raw_encoding.
            len_padding: The length of the padding at the beginning of chunk.
        """
        for i in range(self._chunksize-len_padding):
            self.assertEqual(chunk[-i-1], raw_encoding[-i-1])

    @staticmethod
    def _separate_chunks(
                output: Tuple[torch.LongTensor, torch.LongTensor]
            ) -> Tuple[torch.LongTensor, torch.LongTensor]:
        """Separate samples in output according to index.

        Assumes that output containes only two different indeces.

        Args:
            output: Index and Data tensor as returned by Tensorizer.

        Returns:
            The first set of samples (first index) and the second set of samples
            (second index).
        """
        index = output[0]
        fileindex_a = index[0]
        end = 0
        while(end < len(index) and index[end] == fileindex_a):
            end += 1
        return output[1][:end], output[1][end:]

    def _delete_temp_files(self):
        """Deletes temporary files that were created for testing."""
        self._delete_file(self.temp_dir, self._filename_a)
        self._delete_file(self.temp_dir, self._filename_b)

    _chunksize = 128
    _filename_a = Path(r"A.txt")
    _filename_b = Path(r"B.txt")
    # Line length is irrelevant in this case.
    # pylint: disable=line-too-long
    _file_content = {
        "file_a": """Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum""",
        "file_b": """Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?""",
    }
