import unittest
from typing import List
import numpy as np
from transformers.trainer_utils import EvalPrediction
from training.evaluation.metrics.mrr import mrr, get_top_k_tokens, get_reciprocal_rank


class TestMRR(unittest.TestCase):
    """Tests MRR metric function."""

    def test_mrr(self):
        """Test calculated mrr score."""
        self.assertEqual(
            mrr(
                0.0,
                self._sample_predictions
            ),
            {'mrr': 0.23333333333333334}
        )

    def test_get_top_k_tokens(self):
        """Test get_top_k_tokens helper."""
        self.assertTrue(np.array_equal(
                get_top_k_tokens(
                    np.array(
                        [[
                            [5, 4, 3, 2, 1],
                            [1, 2, 3, 4, 5],
                            [3, 1, 4, 2, 5]
                        ]]
                    ),
                    3
                ),
                np.array([[[0, 1, 2], [4, 3, 2], [4, 2, 0]]])
            ))

    def test_get_reciprocal_rank_found(self):
        """Test get_reciprocal_rank helper when token is among the top k."""
        self.assertEqual(
                get_reciprocal_rank(
                    6,
                    np.array([9, 6, 3])
                ),
                0.5
            )

    def test_get_reciprocal_rank_not_found(self):
        """Test get_reciprocal_rank helper when token is not among the top k."""
        self.assertEqual(
                get_reciprocal_rank(
                    6,
                    np.array([9, 5, 3])
                ),
                0
            )

    @staticmethod
    def _make_array(indices: List[int], values: List[int]):
        """Create zero initialized array of size (52000,) and set only few values.

        Args:
            indices: Indeces of the values contained in values.
            values: Values to placed at the given indeces.

        Returns:
            Numpy array with all elements zero except the indices given which
            will be filled with the given values.
        """
        arr = np.zeros(shape=(52000,))
        arr[indices] = values
        return arr

    _sample_predictions = EvalPrediction(
            predictions=np.array([
                [
                    _make_array.__func__([5], [1]),
                    _make_array.__func__([3], [1]),
                    _make_array.__func__([3], [0.5]),
                    _make_array.__func__([1, 3], [0.5, 0.5]),
                    _make_array.__func__([1], [1])
                ],
                [
                    _make_array.__func__([1], [1]),
                    _make_array.__func__([3, 4], [0.5, 0.5]),
                    _make_array.__func__([3, 4], [0.5, 0.5]),
                    _make_array.__func__([3, 4], [0.5, 0.5]),
                    _make_array.__func__([2], [1])
                ],
                [
                    _make_array.__func__([2, 3], [0.5, 0.5]),
                    _make_array.__func__([1], [1]),
                    _make_array.__func__([3, 5], [0.5, 0.5]),
                    _make_array.__func__([2], [1]),
                    _make_array.__func__([5, 3], [0.5, 0.5])
                ]
                ]),
            label_ids=np.array([
                    [5, 4, 3, 2, 1],
                    [1, 2, 3, 4, 5],
                    [3, 1, 4, 2, 5]
                ])
        )
