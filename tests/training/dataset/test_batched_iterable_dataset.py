from pathlib import Path
import torch
from training.dataset.batched_iterable_dataset import BatchedIterableDataset
from tests.util.test_fileoperations import TestFileoperations


class TestBatchedIterableDataset(TestFileoperations):
    """Tests BatchedIterableDataset class."""

    def setUp(self):
        """Set up test. Will be called before each test."""
        super().setUp()
        self._dataset = BatchedIterableDataset(
            self.temp_dir/self._filename_index,
            self.temp_dir/self._filename_data,
            batch_size=3,
        )

    def test_dataset(self):
        """Test iteration over dataset."""
        # Comprehension turns iterable dataset into list of data samples.
        # Very much necessary.
        # pylint: disable=unnecessary-comprehension
        output = [i for i in self._dataset]
        expected_output = [
            torch.Tensor([[0., 0.], [1., 1.], [2., 2.]]),
            torch.Tensor([[0., 0.], [1., 1.], [2., 2.]]),
            torch.Tensor([[0., 0.], [1., 1.], [2., 2.]]),
            torch.Tensor([[0., 0.], [1., 1.], [2., 2.]]),
            torch.Tensor([[0., 0.], [1., 1.], [2., 2.]]),
            torch.Tensor([[0., 0.], [3., 3.], [2., 2.]]),
            torch.Tensor([[4., 4.], [3., 3.], [2., 2.]]),
            torch.Tensor([[4., 4.], [3., 3.], [5., 5.]])
        ]
        self.assertTrue(len(output) == len(expected_output))
        # Using indeces is much cleaner in this case
        # pylint: disable=consider-using-enumerate
        for i in range(len(output)):
            self.assertTrue(torch.all(torch.eq(output[i], expected_output[i])))

    def _setup_temp_files(self):
        """Sets up temporary files for tests."""
        torch.save(self._file_content["data"], self.temp_dir/self._filename_data)
        torch.save(self._file_content["index"], self.temp_dir/self._filename_index)

    def _delete_temp_files(self):
        """Deletes temporary files that were created for testing."""
        self._delete_file(self.temp_dir, self._filename_data)
        self._delete_file(self.temp_dir, self._filename_index)

    _filename_data = Path(r"data.pt")
    _filename_index = Path(r"index.pt")
    _file_content = {
                    "data":
                    torch.Tensor([
                        [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0],
                        [1, 1], [1, 1], [1, 1], [1, 1], [1, 1],
                        [2, 2], [2, 2], [2, 2], [2, 2], [2, 2], [2, 2], [2, 2],
                        [3, 3], [3, 3], [3, 3],
                        [4, 4], [4, 4], [4, 4], [4, 4],
                        [5, 5], [5, 5], [5, 5], [5, 5], [5, 5], [5, 5]
                    ]),
                    "index":
                    torch.Tensor([
                        0, 0, 0, 0, 0, 0,
                        1, 1, 1, 1, 1,
                        2, 2, 2, 2, 2, 2, 2,
                        3, 3, 3,
                        4, 4, 4, 4,
                        5, 5, 5, 5, 5, 5
                    ])
                }
