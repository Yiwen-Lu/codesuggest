import unittest
from typing import List
from training.dataset.iterable_consumer import IterableConsumer


class TestIterableConsumer(unittest.TestCase):
    """Tests IterableConsumer class."""

    def test_single_consumer(self):
        """Test iteration over one consumer."""
        consumer = IterableConsumer(iter(self._sample_data))
        self.assertEqual(
            # Comprehension turns iterable into list of data samples.
            # Very much necessary.
            # pylint: disable=unnecessary-comprehension
            [i for i in consumer],
            [
                0, 0, 0, 0, 0, 0,
                1, 1, 1, 1, 1,
                2, 2, 2, 2, 2, 2, 2,
                3, 3, 3,
                4, 4, 4, 4,
                5, 5, 5, 5, 5, 5
            ]
        )

    def test_multiple_consumers(self):
        """Test simultanious iteration over multiple consumers."""
        iterators = self._make_iterable_consumer_iterators(number_of_consumers=3)
        number_of_consumers = len(iterators)
        output = [[] for i in range(number_of_consumers)]
        while True:
            try:
                for i in range(number_of_consumers):
                    output[i] += [next(iterators[i])]
            except StopIteration:
                break
        self.assertEqual(
            output,
            [
                [0, 0, 0, 0, 0, 0, 4, 4, 4],
                [1, 1, 1, 1, 1, 3, 3, 3],
                [2, 2, 2, 2, 2, 2, 2, 5]
            ]
        )

    def _make_iterable_consumer_iterators(self, number_of_consumers: int) -> List[IterableConsumer]:
        """Create a list of IterableConsumers with shared raw iterator.

        Args:
            number_of_consumers: Number of consumers to create.

        Returns:
            List of consumers with shared underlying data.
        """
        raw_iterator = iter(self._sample_data)
        return [iter(IterableConsumer(raw_iterator)) for i in range(number_of_consumers)]

    _sample_data = [
                [0, 0, 0, 0, 0, 0],
                [1, 1, 1, 1, 1],
                [2, 2, 2, 2, 2, 2, 2],
                [3, 3, 3],
                [4, 4, 4, 4],
                [5, 5, 5, 5, 5, 5]
            ]
