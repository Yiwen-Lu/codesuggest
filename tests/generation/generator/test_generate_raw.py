import unittest
import os
from typing import List
import torch
from transformers import TransfoXLLMHeadModel
from tokenizer.tokenizer import WordPieceTokenizer


class TestGenerateRaw(unittest.TestCase):
    """Tests the raw ouput from the model.

    These tests require a model and will be skipped if no model  is present.

    The output can't be tested directly beyond ensuring that generation happens
    wihtout throwing an exception. All tests will print their output to allow
    semantical evaluation.
    """

    def setUp(self):
        """Set up test. Will be called before each test."""
        self._tokenizer = WordPieceTokenizer(
                vocab_file=r"tokenizer/PythonP150-vocab.txt",
            )
        self._model = TransfoXLLMHeadModel.from_pretrained(r'model')
        self._model.eval()

    @unittest.skipIf(not os.path.isdir('model') or not os.listdir('model'),
                     "No Model present. Skipped model depend test.")
    def test_top_ten_with_past(self):
        """Test generation of tokens with 10 highest likelihoods. Using past."""
        encoding = self._tokenizer.encode(self._sample_text.replace('\n', '[EOL]'))
        input_ids = torch.tensor([encoding.ids])

        self._print_input(input_ids, include_past=True)
        mems = self._model(input_ids[:, -2*self._sequence_length:-self._sequence_length])[1]
        output = self._model(input_ids[:, -self._sequence_length:], mems=mems)[0]
        token_probs_last_token = output[0, -1]
        predictions = torch.topk(token_probs_last_token, 10)[1]
        self._print_predictions(predictions)

    @unittest.skipIf(not os.path.isdir('model') or not os.listdir('model'),
                     "No Model present. Skipped model depend test.")
    def test_top_ten_without_past(self):
        """Test generation of tokens with 10 highest likelihoods. Not using past."""
        encoding = self._tokenizer.encode(TestGenerateRaw._sample_text.replace('\n', '[EOL]'))
        input_ids = torch.tensor([encoding.ids])

        self._print_input(input_ids, include_past=False)
        mems = None
        output = self._model(input_ids[:, -self._sequence_length:], mems=mems)[0]
        token_probs_last_token = output[0, -1]
        predictions = torch.topk(token_probs_last_token, 10)[1]
        self._print_predictions(predictions)

    @staticmethod
    def _print_input(input_ids: List[int], include_past: bool = False):
        """Prints all tokens used for generation.

        Args:
            input_ids: List of input token ids.
            include_past: If True will print all tokens that are used
                as extended context as well.
        """
        if include_past:
            print("Used past:")
            print(input_ids[
                    :,
                    -2*TestGenerateRaw._sequence_length:-TestGenerateRaw._sequence_length
                ])
        print("Used input:")
        print(input_ids[:, -TestGenerateRaw._sequence_length:])

    @staticmethod
    def _print_predictions(predictions: List[int]):
        """Prints predicted top 10 tokens.

        Args:
            predictions: List of token ids.
        """
        print("Top 10 Predictions")
        print(predictions)

    _sequence_length = 128
    _sample_text = """       # compute attention probability
        if attn_mask is not None and torch.sum(attn_mask).item():
            attn_mask = attn_mask == 1  # Switch to bool
            if attn_mask.dim() == 2:
                if next(self.parameters()).dtype == torch.float16:
                    attn_score = (
                        attn_score.float().masked_fill(attn_mask[None, :, :, None], -65000).type_as(attn_score)
                    )
                else:
                    attn_score = attn_score.float().masked_fill(attn_mask[None, :, :, None], -1e30).type_as(attn_score)
            elif attn_mask.dim() == 3:
                if next(self.parameters()).dtype == torch.float16:
                    attn_score = attn_score.float().masked_fill(attn_mask[:, :, :, None], -65000).type_as(attn_score)
                else:
                    attn_score = attn_score.float().masked_fill(attn_mask[:, :, :, None], -1e30).type_as(attn_score)

        # [qlen x klen x bsz x n_head]
        attn_prob = F.softmax(attn_score, dim=1)
        attn_prob = self.dropatt(attn_prob)

        # Mask heads if we want to
        if head_mask is not None:
            attn_prob = attn_prob * head_mask

        # compute attention vector
        attn_vec = torch.einsum("ijbn,jbnd->ibnd", (attn_prob, w_head_v))

        # [qlen x bsz x n_head x d_head]
        attn_vec = attn_vec.contiguous().view(attn_vec.size(0), attn_vec.size(1), self.n_head * self.d_head)

        # linear projection
        attn_out = self.o_net(attn_vec)
        attn_out = self.drop(attn_out)

        if self.pre_lnorm:
            # residual connection
            outputs = [w + attn_out]
        else:
            # residual connection + layer normalization
            outputs = [self.layer_norm"""
