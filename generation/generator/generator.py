from typing import List, Optional, Tuple
import torch
from transformers import TransfoXLLMHeadModel
from tokenizer.tokenizer import WordPieceTokenizer


class Generator:
    """Generate whole-line recommendations from input strings"""

    def __init__(
        self,
        filepath_to_vocab: str,
        filepath_to_model: str,
        top_k: Optional[int] = 3,
        seq_length: Optional[int] = 128,
        max_num_of_generated_tokens: Optional[int] = 50,
        verbose: Optional[bool] = False
    ):
        """Initializes class with vocab and model files

        Args:
            filepath_to_vocab: Filepath to vocab file used for this model.
            filepath_to_model: Filepath to model file.
            top_k: Number of predictions to generate. (Default: 3)
            seq_length: Number of predictions to generate. (Default: 3)
            max_num_of_generated_tokens: Number of tokens to generate
                for a single prediction before generation is aborted. (Default: 50)
            verbose: If true outputs information about the maximum used
                context as well as intermediary steps during
                generation. (Default: False)

        """
        self._tokenizer = WordPieceTokenizer(
            vocab_file=filepath_to_vocab,
            )

        self._model = TransfoXLLMHeadModel.from_pretrained(filepath_to_model)
        self._model.eval()

        self._k = top_k
        self._seq_length = seq_length
        self._max_num_of_generated_tokens = max_num_of_generated_tokens
        self._verbose = verbose

    def __call__(self, input_str: str) -> List[str]:
        """Generate whole-line recommendations to complete input

        Generates recommendations for how to continue the input string until the
        next line break. Forwards to "generate" method.

        Args:
            input_str: The returned recommendations will be continuations of
                this string.

        Returns:
            Recommendations for how to continue the input string until the
            next line break as list of strings.

        """
        return self.generate(input_str)

    def generate(self, input_str: str) -> List[str]:
        """Generate whole-line recommendations to complete input

        Generates recommendations for how to continue the input string until the
        next line break. Also prints context information and intermediary steps
        if self._verbose == True.

        Args:
            input_str: The returned recommendations will be continuations of
                this string.

        Returns:
            Recommendations for how to continue the input string until the
            next line break as list of strings.

        """
        encoding = self._tokenizer.encode(input_str)
        input_ids = torch.tensor([encoding.ids])

        mems_context_length, mems_context = self._get_mems(
                    input_ids[:, :-self._seq_length]
                )

        mems_context, predicted_token_ids = self._predict_top_k_tokens(
                    input_ids[:, -self._seq_length:],
                    self._k,
                    mems_context
                )

        used_context_length = mems_context_length + self._seq_length
        if self._verbose:
            print("Context: ", encoding.tokens[
                -used_context_length:
                    ])

        predicted_sequences = self._generate_predicted_sequences(
                mems_context,
                predicted_token_ids
            )

        return [
            self._tokenizer.decode(predicted_seq, skip_special_tokens=False)
            for predicted_seq
            in predicted_sequences
            ]

    def _generate_predicted_sequences(
                self,
                mems_context: List[torch.FloatTensor],
                predicted_token_ids: torch.LongTensor
            ) -> List[List[int]]:
        """Generate whole-line recommendations from context and seed tokens

        Generates recommendations by continuing context + token for each token in
        predicted_token_ids until eol-token is generated. Also prints intermediary
        steps if self._verbose == True.

        Args:
            mems_context: The maximal context encoded as model mems.
            predicted_token_ids: List of seed tokens. Each one represents the first
                token of a new predictions.

        Returns:
            Recommendations for how to continue the input string until the
            next line break as a list of lists of token ids.

        """
        predicted_sequences = []
        for predicted_token_id in predicted_token_ids:
            predicted_token_id = predicted_token_id.item()
            mems = mems_context
            predicted_sequence = [predicted_token_id]

            number_of_tokens = 1
            while (predicted_token_id != self._tokenizer.eol_token_id
                    and number_of_tokens < self._max_num_of_generated_tokens):
                mems, predicted_token_id = self._predict_top_token(
                        torch.tensor([[predicted_token_id]]),
                        mems
                    )
                predicted_sequence += [predicted_token_id]
                number_of_tokens += 1
                if self._verbose:
                    print("Predictions: \n", predicted_sequence)

            predicted_sequences += [predicted_sequence]

        return predicted_sequences

    def _predict_top_token(
                self,
                token_ids: torch.LongTensor,
                mems: Optional[List[torch.FloatTensor]] = None
            ) -> Tuple[List[torch.FloatTensor], int]:
        """Predict next token from token ids and mems

        Predicts next tokens as the token with the highest probability according
        to the model when using token_ids and mems as input.

        Args:
            token_ids: The input sequence as token ids.
            mems: The extended context as model mems. (Default: None)

        Returns:
            Mems representation of the sequence used to predict the token.
            Token id of the predicted token.

        """
        mems, predicted_token = self._predict_top_k_tokens(
                token_ids,
                k=1,
                mems=mems
            )
        return mems, predicted_token.item()

    def _predict_top_k_tokens(
                self,
                token_ids: torch.LongTensor,
                k: Optional[int] = 5,
                mems: Optional[List[torch.FloatTensor]] = None
            ) -> Tuple[List[torch.FloatTensor], torch.LongTensor]:
        """Predict the k tokens most likely to be the next token from token ids and mems

        Predicts next tokens as the k tokens with the highest probability according
        to the model when using token_ids and mems as input.

        Args:
            token_ids: The input sequence as token ids.
            k: Number of tokens to return.
            mems: The extended context as model mems. (Default: None)

        Returns:
            Mems representation of the sequence used to predict the token.
            Token ids of the predicted tokens.

        """
        output = self._model(
                input_ids=token_ids,
                mems=mems
            )
        token_probs_last_token = output[0][0, -1]
        mems = output[1]
        return mems, torch.topk(token_probs_last_token, k)[1]

    def _get_mems(
                self,
                token_ids: torch.LongTensor
            ) -> Tuple[int, List[torch.FloatTensor]]:
        """Return the mems representation of the given token ids

        Returns the mems representation of the given token ids. The length is
        limited to the maximal mems length of the model. Only complete chunks are
        returned if mems length is no multiple of sequence length. Also returns
        the actual number of tokens embedded in the mems. (This is the biggest
        multiple of sequence length that is smaller than the maximal mems length)
        (mems always has the same length and is padded with zeros)

        Args:
            token_ids: The input sequence as token ids.

        Returns:
            Actual numer of tokens embedded in mems.
            Mems representation of the token ids.

        """
        max_past_length = min(self._model.transformer.mem_len, token_ids.size(1))
        num_of_chunks = max_past_length//self._seq_length
        furthest_past_token = -self._seq_length*num_of_chunks
        past_chunks = [
                    token_ids[
                        :,
                        furthest_past_token+self._seq_length*i:
                        furthest_past_token+self._seq_length*(i+1)
                            if i+1 != num_of_chunks else None
                        ]
                    for i in range(num_of_chunks)
                ]
        return -furthest_past_token, self._get_mems_from_chunks(past_chunks)

    def _get_mems_from_chunks(
                self,
                past_chunks: List[torch.LongTensor]
            ) -> List[torch.FloatTensor]:
        """Return the mems representation of the given chunks of token ids

        Returns the mems representation of the given token ids. The token ids
        are given as chunks of sequence length and can thus be passed into the
        model chunkswise.

        Args:
            past_chunks: The input sequence as chunks of token ids (Each chunk
            has sequence length many tokens).

        Returns:
            Mems representation of the token ids.

        """
        mems = None
        for chunk in past_chunks:
            mems = self._model(
                            input_ids=chunk,
                            mems=mems
                    )[1]
        return mems
