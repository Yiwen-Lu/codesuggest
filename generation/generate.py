"""Script to generate whole-line predictions from model

Basically CLI wrapper for Generator class
"""
import argparse
from generation.generator.generator import Generator
from generation.samples import samples


def main(args):
    """Generates the top 3 whole-line predictions and prints those and the used input.

    Args:
        args: Parsed commandline arguments.

    """
    generator = Generator(
            filepath_to_vocab=args.filepath_to_vocab,
            filepath_to_model=args.filepath_to_model,
            verbose=args.verbose
        )

    print("Input: ", args.input)
    print("Output: ")

    rank = 1
    for predicted_seq in generator(args.input.replace('\n', '[EOL]')):
        print("{}. ".format(rank) + predicted_seq)
        rank += 1


def parse_arguments():
    """Parses the commandline arguments.

    Returns:
        Arguments extracted from the commandline.

    """
    parser = argparse.ArgumentParser(
            description='Generates the top 3 whole-line predictions \
                and prints those and the used input.'
        )
    parser.add_argument('--filepath_to_vocab', type=str,
                        default='tokenizer/PythonP150-vocab.txt',
                        help='path to vocab txt file')
    parser.add_argument('--filepath_to_model', type=str,
                        default=r'model',
                        help='path to directory of model')
    parser.add_argument('--input', type=str,
                        default=None,
                        help='input code snippet for testing')
    parser.add_argument('--sample', type=int,
                        default=3,
                        help='use one of the provided samples')
    parser.add_argument('--verbose', default=False, action='store_true',
                        help='Output additional debug information')

    args = parser.parse_args()

    if args.input is None:
        args.input = samples[args.sample]

    return args


if __name__ == '__main__':
    main(parse_arguments())
