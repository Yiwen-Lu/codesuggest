# CodeSuggest
The goal of the CodeSuggest project is to provide a Transformer-XL based whole-line code recommender. While no IDE-Integration has been achieved the provided Generator class offers an easy to use interface to generate code recommendations. The project is based in large parts on the structure offered by the Huggingface library.

The final version of the model can be found here:
https://drive.google.com/drive/folders/16lhecS5zv7xlZibad-R92pf7cA4zGBRA?usp=sharing

A virtual machine image containing this repository, the model files, the dataset and an appropriately set up environment can be found here:
https://drive.google.com/drive/folders/19lx3MpqYS_xPcCVIjbmAKnJQIWZOyP2_?usp=sharing

# Repo Structure
This Repository contains all classes and scripts used to train and evaluate the Transformer-XL model, as well as those used to facilitate generation using a trained model.

## Docs
This Folder contains all the presentations and the pair-programming protocol.

## Generation
This folder contains the Generator class. Once initiated with a trained model the Generator exposes the following generate method: <p>generate(input_str: str) -> List[str]</p> To generate predictions simply pass the code as string. The method will return a list of predictions for how to continue the code up to the next line-break. The Generator's __call__ method forwards to the generate method and can be used in its place.

To play around with the Generator use the generate.py script in the parent folder. This script offers a command line interface to the Generator. Samples are included and can be called with --sample [num]

## py150
Some of the files in the py150 dataset, which was used to train the model, cause problems when training the Tokenizer. This folder contains edited versions of the indexfiles from py150. Problematic files have been commented out. Should any files make problems on your system you can comment them out by prepending the relevant line with #.

## Tests
This folder contains tests for the classes in the rest of the project. The folder structure mirrors that of the project as a whole. Some tests require a trained model, these will be skipped unless the model files are placed in a top level folder "model".

## Tokenizer
This folder contains the tokenizer used for the model. The tokenizer is a version of the WordPiece tokenizer that never merges punctuation. To train the tokenizer on a new corpus run the training script.

## Training
This folder contains all the classes and scripts necessary to successfully train and evaluate a new Tranformer-XL model. 

### Collater
This folder contains collaters for the random access and iterable datasets.

### Dataset
This folder contains one dataset with random access that can be used for training without the past. It also contains an iterable dataset that provides sequential access to the underlying data and can be used to train with the past. The iterable dataset can be thought of as a set of channels. Each index within a batch maps to one channel and within one channel samples are passed sequentially. For example, if working with batch size 6 there are 6 different channels, i.e. the first sample of batch two is the element that follows the first sample of batch one. The important part is that this works with data that is logically grouped as well. One channel will sequencially work through one group of data, while another channel consumes a different group of data. In our case the logical groups are based on the files the data belongs to. This means the chunks of tokenized code from one file will be passed to the model in sequencial order.

### Evaluation
This folder contains the evaluation script and defines the MRR and Perplexity metric.

### Preprocessing
This folder contains scripts to turn the py150 into a pair of tensors containing a tokenized and chunked versions of the data, as well as the corresponding file index. 

### Trainer
This folder contains a modified version of the Huggingface Trainer class that was used for the model.

# Colab
The scripts used to train/evaluate/preprocess on google colab are included throughout the project.

# Where to pick up
For anyone interested in picking up the project here are a couple of ideas where to begin.

## IDE Integration
The obvious way to continue the project would be to finish integration with an IDE, for example by implementing the language server protocol.

## Performance
One problem with the current version of the model is performance. The model is so big that training is very hardware intensive which meant we could never use the entirety of the available data. Furthermore, the generation of predictions takes far too long, especially without GPU acceleration, to be feasably used in a production environment. Possible ways to around this problem include downsizing the model or setting up a server based system.

## Evaluation
The default Huggingface evaluation script collects all model output first before passing it on to calculate metrics. This means memory consumption grows linearly with the number of evaluation samples. This puts a limit on the maximum size of the evaluation dataset. However, as this behaviour is intrinsic to Huggingface's approach to evaluation it requires setting up an independent evaluation loop to overcome. For consistency's sake we chose to remain faithful to the Huggingface library for the time being, nonetheless, you might consider a different approach.

# Credit
Planning and Presentations - Yiwen Lu and Lennart Stöpler <br>
Implementation - Lennart Stöpler
